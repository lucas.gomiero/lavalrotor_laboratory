import math


#länge der welle in m
l2 = 0.75
l3 = 0.75
l4 = 0.75

#abstand der scheiben in m (ist differenz der lagerungen)
s2 = 0.5 - 0.25
s3 = 0.45
s4 = 0.45

#E-modul der welle in N/m**2
E2 = 210000*10**6
E3 = 210000*10**6
E4 = 210000*10**6

#durchmesser der Welle in m
d2 = 0.008
d3 = 0.010
d4 = 0.008

#masse der Scheibe in kg
m2 = 0.5
m3 = 0.95
m4 = 1

#y
y2 = s2/l2
y3 = s3/l3
y4 = s4/l4

#a_1 und a_2
a1_2 = 1 - 2*y2*y2 + y2*y2*y2*y2
a2_2 = 1 - 4*y2*y2 + 4*y2*y2*y2 - y2*y2*y2*y2

a1_3 = 1 - 2*y3*y3 + y3*y3*y3*y3
a2_3 = 1 - 4*y3*y3 + 4*y3*y3*y3 - y3*y3*y3*y3

a1_4 = 1 - 2*y4*y4 + y4*y4*y4*y4
a2_4 = 1 - 4*y4*y4 + 4*y4*y4*y4 - y4*y4*y4*y4

#flächenträgheiitsmoment berech
I2 = math.pi*d2*d2*d2*d2/64
I3 = math.pi*d3*d3*d3*d3/64
I4 = math.pi*d4*d4*d4*d4/64

#ersatzfedersteifigkeit k berechnen in N/m
k2 = 48*E2*I2/(l2*l2*l2)
k3 = 48*E3*I3/(l3*l3*l3)
k4 = 48*E4*I4/(l4*l4*l4)

#kritische kreisfrequenzen in 1/min
w1_2 = math.sqrt(k2/(m2*(a1_2 + a2_2)))*60/(2*math.pi)
w2_2 = math.sqrt(k2/(m2*(a1_2 - a2_2)))*60/(2*math.pi)

w3 = math.sqrt(k3/m3)*60/(2*math.pi)


w4 = math.sqrt(k4/m4)*60/(2*math.pi)

print(w1_2)
print(w2_2)
print()
print(w1_3)
print(w2_3)
print()
print(w1_4)
print(w2_4)